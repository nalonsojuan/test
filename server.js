const express = require("express");
const bodyparser = require("body-parser");
const knex = require("./db/knex");

const app = express();
const PORT = process.env.PORT || 1234;

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

console.log("THIS IS A CHANGE!!!");

// USERS
app.get("/users", (req, res) => {
  knex
    .select()
    .from("users")
    // .raw("SELECT * FROM users;")
    .then((query) => res.send({ data: query /*.rows*/ }))
    .catch((err) => res.status(500).send(err));
});
app.get("/users/:id", (req, res) => {
  knex
    .select()
    .from("users")
    .where("id", req.params.id)
    .then((query) => res.send({ data: query }))
    .catch((err) => res.status(500).send(err));
});
app.get("/users/:id/todos", (req, res) => {
  knex
    .select()
    .from("users")
    .innerJoin("todos", "users.id", "todos.user_id")
    .where("users.id", req.params.id)
    // .raw(
    //   "SELECT * FROM users INNER JOIN todos ON users.id = todos.user_id WHERE users.id = ?;",
    //   [req.params.id]
    // )
    .then((query) => res.send(query /*.rows*/));
});

// TODOS
app.get("/todos", (req, res) => {
  knex
    .select()
    .from("todos")
    // .raw("SELECT * FROM todos;")
    .then((query) => res.send({ data: query /*.rows*/ }))
    .catch((err) => res.status(500).send(err));
});
app.get("/todos/:id", (req, res) => {
  knex
    .select()
    .from("todos")
    .where("id", req.params.id)
    .then((query) => res.send({ data: query }))
    .catch((err) => res.status(500).send(err));
});
app.post("/todos", (req, res) => {
  knex("todos")
    .insert({
      title: req.body.title,
      user_id: req.body.user_id,
    })
    .returning("*")
    // .raw("INSERT INTO todos(title, user_id) VALUES (?, ?) RETURNING *;", [
    //   "Some house chore",
    //   "1",
    // ])
    .then((query) => res.send(query));
});
app.put("/todos/:id", (req, res) => {
  knex("todos")
    .where("id", req.params.id)
    .update({
      title: req.body.title,
      completed: req.body.completed,
    })
    .returning("*")
    // .raw("UPDATE todos SET " + req.body.field + "= ? WHERE id = ?;", [
    //   req.body.value,
    //   req.params.id,
    // ])
    .then((query) => {
      res.send(query);
    });
});
app.delete("/todos/:id", (req, res) => {
  knex("todos")
    .where("id", req.params.id)
    .delete()
    .returning("*")
    .then((query) => res.send(query));
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}.`);
});
