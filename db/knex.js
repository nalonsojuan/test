const environment = process.env.NODE_ENV || "development";
const config = require("../knexfile")[environment];

const something = require("knex");

module.exports = something(config);
