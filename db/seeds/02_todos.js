/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("todos").del();
  await knex("todos").insert([
    { title: "chore 1", completed: false, user_id: 1 },
    { title: "chore 2", completed: false, user_id: 2 },
    { title: "chore 3", completed: false, user_id: 3 },
  ]);
};
