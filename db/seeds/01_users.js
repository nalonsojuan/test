/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("users").del();
  await knex("users").insert([
    { name: "tupac", email: "tupac@gmail.com" },
    { name: "someone", email: "someone@blabla.com" },
    { name: "test", email: "test@test.test" },
  ]);
};
